/*
 * Copyright (C) 2019 Daniél Kerkmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


$(function () {
    function renderLobbys(data) {
        let buffer = "<li class='list-group-item list-group-item-warning'>Lobby</li>";
        for (let listitem in data) {
            if (data.hasOwnProperty(listitem)) {
                if (currentGame !== undefined && data[listitem].uuid === currentGame.uuid) {
                    buffer += "<li id='" + data[listitem].uuid + "' class='list-group-item list-group-item-success'>" + data[listitem].name + " (" + data[listitem].playerSystem['players'].length + "/" + data[listitem].playerSystem['maxPlayers'] + ")</li>";
                } else if (data[listitem].ready) {
                    buffer += "<li id='" + data[listitem].uuid + "' class='list-group-item list-group-item-danger'>" + data[listitem].name + " (" + data[listitem].playerSystem['players'].length + "/" + data[listitem].playerSystem['maxPlayers'] + ")</li>";
                } else {
                    buffer += "<li id='" + data[listitem].uuid + "' class='list-group-item list-group-item-primary clickable'>" + data[listitem].name + " (" + data[listitem].playerSystem['players'].length + "/" + data[listitem].playerSystem['maxPlayers'] + ") <button type='button' class='btn btn-outline-success btn-sm right' data-toggle='modal' data-target='#joinGameModal' data-uuid='" + listitem + "'>Join</button></li>";
                }
            }
        }

        $('#lobbylist').html(buffer);
    }

    function renderPlayer(data) {
        let buffer = "<li class='list-group-item list-group-item-warning'>Player</li>";

        for (let listitem in data) {
            if (data.hasOwnProperty(listitem)) {
                buffer += "<li id='" + data[listitem].uuid + "' class='list-group-item'>" + data[listitem].username;
                buffer += " (<span>0</span>)";
                buffer += "</li>";
            }
        }

        $('#playerlist').html(buffer);
    }

    function renderGame(data) {
        let rows = data.board.height * 2 - 1;
        let col = data.board.width * 2 - 1;

        let buffer = "<div class=\"container-fluid gamebox\">";

        for (let y = 0; y < rows; y++) {
            buffer += "<div class='row' style='height: " + 100 / rows + "%; align-items: center; text-align: center;'>";
            for (let x = 0; x < col; x++) {

                if (y % 2 === 0 && x % 2 !== 0) {
                    buffer += "<div class='col paddingzero' style='width: 100%; height: 20px;'>";
                } else if (y % 2 !== 0 && x % 2 === 0) {
                    buffer += "<div class='col paddingzero' style='width: 20px; height: 100%;'>";
                } else {
                    buffer += "<div class='col paddingzero' style='width: 20px; height: 20px;'>";
                }

                if (y % 2 === 0 && x % 2 !== 0) {
                    buffer += "<div class='line-horizontal lines' data-x='" + x + "' data-y='" + y + "'></div>";
                } else if (y % 2 !== 0 && x % 2 === 0) {
                    buffer += "<div class='line-vertical lines' data-x='" + x + "' data-y='" + y + "'></div>";
                } else if (y % 2 !== 0 && x % 2 !== 0) {
                    buffer += "<i class='far fa-circle rewardcircle' data-x='" + x + "' data-y='" + y + "' style='color: black;'></i>"
                } else {
                    buffer += "<i class='fas fa-circle nonrewardcircle' style='color: lightgray;'></i>"
                }
                buffer += "</div>";
            }
            buffer += "</div>"
        }
        buffer += "</div>";

        $('#gamecol').html(buffer);
    }

    function renderError(data) {
        let buffer = "";
        buffer += "<p>" + data + "</p>";
        $('#lobbys').html(buffer);
    }

    function renderCurrentPlayer(data) {
        $("#playerlist").find('li').removeClass('list-group-item-success');
        $("#" + data.uuid).addClass('list-group-item-success').removeClass('list-group-item-warning');
    }

    function renderMoves(data) {
        for (let item in data) {
            if (data.hasOwnProperty(item)) {
                let coordinatesarr = data[item];
                for (let i = 0; i < coordinatesarr.length; i++) {
                    let coordinates = coordinatesarr[i];
                    let x = coordinates.x;
                    let y = coordinates.y;
                    if (y % 2 === 0 && x % 2 !== 0) {
                        $('.lines[data-x="' + x + '"][data-y="' + y + '"]').css('background-color', 'green');
                    } else if (y % 2 !== 0 && x % 2 === 0) {
                        $('.lines[data-x="' + x + '"][data-y="' + y + '"]').css('background-color', 'green');
                    }
                }
            }
        }
    }

    function renderRewards(data) {
        for (let item in data) {
            if (data.hasOwnProperty(item)) {
                let coordinatesarr = data[item];
                $("#" + item).find('span').html(data[item].length);
                for (let i = 0; i < coordinatesarr.length; i++) {
                    let coordinates = coordinatesarr[i];
                    let x = coordinates.x;
                    let y = coordinates.y;
                    $('.rewardcircle[data-x="' + x + '"][data-y="' + y + '"]').replaceWith("<i class='fas fa-circle rewardcircle' data-x='" + x + "' data-y='" + y + "' style='color: green;'></i>");
                }
            }
        }
    }

    function renderStats(data) {
        let buffer = '';
        console.log(data);
        for (let i = 0; i < data.length; i++) {
            buffer += "<tr>";
            buffer += "<td>" + data[i].player.username + "</td>";
            buffer += "<th scope='row'>" + data[i].rewards.length + "</th>";
            buffer += "</tr>";
        }
        $('#stats').html(buffer);
    }

    function getGames() {
        $.ajax({
            url: 'http://localhost:8080/games/all',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                renderLobbys(data);
            },
            error: function (jqXHR) {
                renderError(jqXHR.responseJSON.message);
            }
        });
    }

    function getPlayer() {
        $.ajax({
            url: 'http://localhost:8080/games/players/all',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                renderPlayer(data);

                getCurrentPlayer();
            },
            error: function (jqXHR) {
                renderError(jqXHR.responseJSON.message);
            }
        });
    }

    function createGame() {
        $.ajax({
            url: 'http://localhost:8080/games',
            type: 'POST',
            data: {
                "width": $('#width').val().trim(),
                "height": $('#height').val().trim(),
                "max_players": $('#max_players').val().trim(),
                "username": $('#username').val().trim(),
                "name": $('#boardname').val().trim()
            },
            dataType: 'json',
            success: function (data) {
                initBoard(data);
            },
            error: function (jqXHR) {
                renderError(jqXHR.responseJSON.message);
            }
        });
    }

    function joinGame() {
        $.ajax({
            url: 'http://localhost:8080/games',
            type: 'PUT',
            data: {
                "game_uuid": $('#hiddenuuid').text().trim(),
                "username": $('#username2').val().trim()
            },
            dataType: 'json',
            success: function (data) {
                initBoard(data);
            },
            error: function (jqXHR) {
                renderError(jqXHR.responseJSON.message);
            }
        });
    }

    function getCurrentPlayer() {
        $.ajax({
            url: 'http://localhost:8080/games/players/current',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                renderCurrentPlayer(data);
            },
            error: function (jqXHR) {
                renderError(jqXHR.responseJSON.message);
            }
        });
    }

    function getCurrentGame() {
        $.ajax({
            url: 'http://localhost:8080/games',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                initBoard(data);
            },
            error: function (jqXHR) {
                renderError(jqXHR.responseJSON.message);
            }
        });
    }

    function getMoves() {
        $.ajax({
            url: 'http://localhost:8080/games/moves',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                renderMoves(data);
            },
            error: function (jqXHR) {
                renderError(jqXHR.responseJSON.message);
            }
        });
    }

    function getRewards() {
        $.ajax({
            url: 'http://localhost:8080/games/rewards',
            type: 'GET',
            dataType: 'json',
            success: function (data) {
                renderRewards(data);
            },
            error: function (jqXHR) {
                renderError(jqXHR.responseJSON.message);
            }
        });
    }

    function addMove() {
        $.ajax({
            url: 'http://localhost:8080/games/moves',
            type: 'POST',
            data: {
                "x": $(this).data('x'),
                "y": $(this).data('y')
            },
            dataType: 'json',
            error: function (jqXHR) {
                renderError(jqXHR.responseJSON.message);
            }
        });
    }

    let currentGame;

    function initBoard(game) {
        currentGame = game;

        renderGame(game);

        getGames();

        getPlayer();
        getMoves();
        getRewards();

        addGameEventsListener();
    }

    function addClickListener() {
        $('#createGameForm').on('submit', (event) => {
            event.preventDefault();
            createGame();
            $('#createGameModal').modal('hide');
        });

        $('#joinGameForm').on('submit', () => {
            event.preventDefault();
            joinGame();
            $('#joinGameModal').modal('hide');
        });

        $(document).on('click', '.lines', addMove);

        $('#joinGameModal').on('show.bs.modal', function (event) {
            $(this).find('.modal-body p').text($(event.relatedTarget).data('uuid'));
        });
    }

    function addGlobalEventsListener() {
        let events = new EventSource('http://localhost:8080/events');

        events.addEventListener("updatesGamesList", (event) => {
            renderLobbys($.parseJSON(event.data).games);
        });
    }

    function addGameEventsListener() {
        let events = new EventSource('http://localhost:8080/events/games');

        events.addEventListener("updatePlayersList", (event) => {
            renderPlayer($.parseJSON(event.data).players);
            renderCurrentPlayer($.parseJSON(event.data).currentPlayer);
        });

        events.addEventListener("addMove", (event) => {
            renderMoves($.parseJSON(event.data).moves);
            renderRewards($.parseJSON(event.data).rewards);
            renderCurrentPlayer($.parseJSON(event.data).currentPlayer);
        });

        events.addEventListener("isComplete", (event) => {
            $('#gameCompleteModal').modal('show');
            renderStats($.parseJSON(event.data).rewards);
        });
    }

    addClickListener();
    addGlobalEventsListener();

    getGames();
    getCurrentGame();
});