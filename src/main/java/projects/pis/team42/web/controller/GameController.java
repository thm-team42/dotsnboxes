/*
 * Copyright (C) 2019 Daniél Kerkmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package projects.pis.team42.web.controller;

import io.javalin.Context;
import projects.pis.team42.domain.Player;
import projects.pis.team42.domain.service.GameService;
import projects.pis.team42.domain.service.PlayerService;
import projects.pis.team42.game.Game;
import projects.pis.team42.utils.GameException;
import projects.pis.team42.utils.SessionUtil;

import java.util.UUID;

public class GameController {

    public static void createGame(Context ctx) {
        try {
            UUID gameUUID = SessionUtil.getUUID(ctx, "game_uuid");
            UUID playerUUID = SessionUtil.getUUID(ctx, "player_uuid");
            PlayerService.getInstance().removePlayer(gameUUID, playerUUID);
        } catch (GameException ignore) {
        }

        UUID gameUUID = UUID.randomUUID();
        UUID playerUUID = UUID.randomUUID();

        Game game = GameService.getInstance().createNewGame(
                gameUUID,
                ctx.formParam("name", ""),
                ctx.formParam("width", Integer.class).get(),
                ctx.formParam("height", Integer.class).get(),
                ctx.formParam("max_players", Integer.class).get(),
                new Player(playerUUID, ctx.formParam("username", String.class).get())
        );

        ctx.sessionAttribute("game_uuid", gameUUID);
        ctx.sessionAttribute("player_uuid", playerUUID);

        ctx.json(game);
    }

    public static void joinGame(Context ctx) {
        try {
            UUID gameUUID = SessionUtil.getUUID(ctx, "game_uuid");
            UUID playerUUID = SessionUtil.getUUID(ctx, "player_uuid");
            PlayerService.getInstance().removePlayer(gameUUID, playerUUID);
        } catch (GameException ignore) {
        }

        UUID gameUUID = ctx.formParam("game_uuid", UUID.class).get();

        UUID playerUUID = UUID.randomUUID();

        Game game = GameService.getInstance().joinExistingGame(
                gameUUID,
                new Player(playerUUID, ctx.formParam("username", String.class).get())
        );

        ctx.sessionAttribute("game_uuid", gameUUID);
        ctx.sessionAttribute("player_uuid", playerUUID);

        ctx.json(game);
    }

    public static void isReady(Context ctx) {
        ctx.json(GameService.getInstance().isReady(SessionUtil.getUUID(ctx, "game_uuid")));
    }

    public static void isCompleted(Context ctx) {
        ctx.json(GameService.getInstance().isCompleted(SessionUtil.getUUID(ctx, "game_uuid")));
    }

    public static void deleteGame(Context ctx) {
        GameService.getInstance().deleteGame(SessionUtil.getUUID(ctx, "game_uuid"));
        ctx.status(200);
    }

    public static void getGames(Context ctx) {
        ctx.json(GameService.getGames());
    }

    public static void getGame(Context ctx) {
        UUID gameUUID = SessionUtil.getUUID(ctx, "game_uuid");
        ctx.json(GameService.getGame(gameUUID));
    }
}
