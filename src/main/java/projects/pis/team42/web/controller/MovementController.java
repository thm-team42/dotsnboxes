/*
 * Copyright (C) 2019 Daniél Kerkmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package projects.pis.team42.web.controller;

import io.javalin.Context;
import io.javalin.rendering.template.TemplateUtil;
import projects.pis.team42.domain.Line;
import projects.pis.team42.domain.service.MovementService;
import projects.pis.team42.utils.SessionUtil;

public class MovementController {

    public static void addMove(Context ctx) {
        MovementService.getInstance().addMove(
                SessionUtil.getUUID(ctx, "game_uuid"),
                SessionUtil.getUUID(ctx, "player_uuid"),
                new Line(
                        ctx.formParam("x", Integer.class).get(),
                        ctx.formParam("y", Integer.class).get()
                )
        );
        ctx.json(TemplateUtil.model());
        ctx.status(201);
    }

    public static void getMoves(Context ctx) {
        ctx.json(MovementService.getInstance().getMoves(SessionUtil.getUUID(ctx, "game_uuid")));
    }
}
