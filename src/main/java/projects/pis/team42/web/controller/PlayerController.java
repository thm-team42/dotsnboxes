/*
 * Copyright (C) 2019 Daniél Kerkmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package projects.pis.team42.web.controller;

import io.javalin.Context;
import projects.pis.team42.domain.service.PlayerService;
import projects.pis.team42.utils.SessionUtil;

import java.util.UUID;

public class PlayerController {

    public static void getPlayer(Context ctx) {
        UUID gameUUID = SessionUtil.getUUID(ctx, "game_uuid");
        UUID playerUUID = SessionUtil.getUUID(ctx, "player_uuid");

        ctx.json(PlayerService.getInstance().getPlayer(gameUUID, playerUUID));
    }

    public static void getPlayers(Context ctx) {
        ctx.json(PlayerService.getInstance().getPlayers(SessionUtil.getUUID(ctx, "game_uuid")));
    }

    public static void getCurrentPlayer(Context ctx) {
        ctx.json(PlayerService.getInstance().getCurrentPlayer(SessionUtil.getUUID(ctx, "game_uuid")));
    }
}
