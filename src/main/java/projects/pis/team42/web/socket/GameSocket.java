/*
 * Copyright (C) 2019 Daniél Kerkmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package projects.pis.team42.web.socket;

import io.javalin.serversentevent.SseClient;
import projects.pis.team42.domain.service.SocketGameService;
import projects.pis.team42.domain.service.SocketGlobalService;
import projects.pis.team42.utils.SessionUtil;

import java.util.UUID;

public class GameSocket {
    public static void onConnectGame(SseClient sseClient) {
        UUID gameUUID = SessionUtil.getUUID(sseClient.ctx, "game_uuid");
        SocketGameService.getInstance().addClient(sseClient, gameUUID);

        sseClient.onClose(() -> SocketGameService.getInstance().removeClient(gameUUID, sseClient));
    }

    public static void onConnect(SseClient sseClient) {
        SocketGlobalService.getInstance().addClient(sseClient);

        sseClient.onClose(() -> SocketGlobalService.getInstance().removeClient(sseClient));
    }
}
