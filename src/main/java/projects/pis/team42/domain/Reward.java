/*
 * Copyright (C) 2019 Daniél Kerkmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package projects.pis.team42.domain;

import java.awt.*;

/**
 * Simple copy of the java.awt.Point class, with a new name
 * Used to represent and store the coordinates of a Reward
 * on the board.
 * Rewards are the boxes that a player can close to gain points.
 */
public class Reward extends Point {
    public Reward(int x, int y) {
        super(x, y);
    }
}
