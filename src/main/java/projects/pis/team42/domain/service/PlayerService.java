/*
 * Copyright (C) 2019 Daniél Kerkmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package projects.pis.team42.domain.service;

import io.javalin.json.JavalinJson;
import io.javalin.rendering.template.TemplateUtil;
import projects.pis.team42.domain.Player;
import projects.pis.team42.game.Game;

import javax.inject.Singleton;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Service class for interaction between
 * the PlayerSystem class and the PlayerController
 */
@Singleton
public class PlayerService {

    @NotNull
    private static PlayerService instance;

    private PlayerService() {
    }

    public static PlayerService getInstance() {
        return instance = Optional.ofNullable(instance).orElse(new PlayerService());
    }

    public Player getPlayer(UUID gameUUID, UUID playerUUID) {
        return GameService.getGame(gameUUID).getPlayerSystem().getPlayer(playerUUID);
    }

    /**
     * Get all players from game
     *
     * @param gameUUID game uuid
     * @return players
     */
    public List<Player> getPlayers(UUID gameUUID) {
        return GameService.getGame(gameUUID).getPlayerSystem().getPlayers();
    }

    /**
     * Get the current player which is doing his turn.
     *
     * @param gameUUID game uuid
     * @return the current playing player
     */
    public Player getCurrentPlayer(UUID gameUUID) {
        return GameService.getGame(gameUUID).getPlayerSystem().getCurrentPlayer();
    }

    /**
     * Remove player from game and delete the game if the room is empty
     *
     * @param gameUUID   game uuid
     * @param playerUUID player uuid
     */
    public void removePlayer(UUID gameUUID, UUID playerUUID) {
        Game game = GameService.getGame(gameUUID);
        game.getPlayerSystem().removePlayer(playerUUID);

        if (game.getPlayerSystem().getPlayers().size() <= 0) {
            GameService.getInstance().deleteGame(gameUUID);
        } else {
            SocketGameService.getClients(gameUUID).forEach(client -> client.sendEvent(
                    "updatePlayersList", JavalinJson.toJson(
                            TemplateUtil.model(
                                    "players", game.getPlayerSystem().getPlayers(),
                                    "currentPlayer", game.getPlayerSystem().getCurrentPlayer()
                            )
                    )
            ));
        }
    }
}
