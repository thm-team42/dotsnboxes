/*
 * Copyright (C) 2019 Daniél Kerkmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package projects.pis.team42.domain.service;

import io.javalin.serversentevent.SseClient;

import javax.inject.Singleton;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Singleton
public class SocketGlobalService {

    private static SocketGlobalService instance;

    @NotNull
    private Set<SseClient> clients;

    private SocketGlobalService() {
        clients = new HashSet<>();
    }

    public static SocketGlobalService getInstance() {
        return instance = Optional.ofNullable(instance).orElse(new SocketGlobalService());
    }

    /**
     * returns all clients
     *
     * @return allClients
     */
    public static Set<SseClient> getClients() {
        return Arrays.stream(getInstance().clients.toArray(new SseClient[0])).collect(Collectors.toSet());
    }

    /**
     * adds client to allClients
     *
     * @param sseClient sseClient to add
     */
    public void addClient(SseClient sseClient) {
        clients.add(sseClient);
    }

    /**
     * removes client from allClients
     *
     * @param sseClient sseClient to remove
     */
    public void removeClient(SseClient sseClient) {
        clients.remove(sseClient);
    }
}
