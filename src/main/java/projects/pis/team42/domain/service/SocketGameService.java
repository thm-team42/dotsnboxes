/*
 * Copyright (C) 2019 Daniél Kerkmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package projects.pis.team42.domain.service;

import io.javalin.serversentevent.SseClient;
import projects.pis.team42.utils.GameException;

import javax.inject.Singleton;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

@Singleton
public class SocketGameService {

    private static SocketGameService instance;

    @NotNull
    private Map<UUID, Set<SseClient>> clients;

    private SocketGameService() {
        clients = new HashMap<>();
    }

    public static SocketGameService getInstance() {
        return instance = Optional.ofNullable(instance).orElse(new SocketGameService());
    }

    /**
     * returns all clients
     *
     * @return allClients
     */
    public static Set<SseClient> getClients(UUID gameUUID) {
        return Arrays.stream(getInstance().clients.get(gameUUID).toArray(new SseClient[0])).collect(Collectors.toSet());
    }

    /**
     * adds a sseClient to a game
     * and to the set of clients in Service
     *
     * @param sseClient sseClient to add
     * @param gameUUID  game to add the player to
     */
    public void addClient(SseClient sseClient, UUID gameUUID) {
        if (!GameService.getGames().containsKey(gameUUID))
            throw new GameException("There is no game.");

        clients.putIfAbsent(gameUUID, new HashSet<>());
        clients.get(gameUUID).add(sseClient);
    }

    /**
     * removes client from allClients
     *
     * @param sseClient sseClient to remove
     */
    public void removeClient(UUID gameUUID, SseClient sseClient) {
        clients.get(gameUUID).remove(sseClient);
    }

    public void removeClient(UUID gameUUID) {
        clients.remove(gameUUID);
    }
}
