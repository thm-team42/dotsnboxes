/*
 * Copyright (C) 2019 Daniél Kerkmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package projects.pis.team42.domain.service;

import io.javalin.json.JavalinJson;
import io.javalin.rendering.template.TemplateUtil;
import projects.pis.team42.domain.Board;
import projects.pis.team42.domain.Player;
import projects.pis.team42.game.Game;
import projects.pis.team42.utils.GameException;

import javax.inject.Singleton;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * Service for interaction between the Game
 * class and the Game Controller.
 * Stores a map of gameUUID and game-instances,
 * containing all running games
 */
@Singleton
public class GameService {

    @NotNull
    private static GameService instance;

    @NotNull
    private Map<UUID, Game> games;

    private GameService() {
        games = new HashMap<>();
    }

    public static GameService getInstance() {
        return instance = Optional.ofNullable(instance).orElse(new GameService());
    }

    /**
     * Get all games.
     *
     * @return all games
     */
    public static Map<UUID, Game> getGames() {
        return getInstance().games;
    }

    /**
     * Get existing game.
     *
     * @param gameUUID uuid of the game you want to access
     * @return running game
     */
    public static Game getGame(UUID gameUUID) {
        if (!getGames().containsKey(gameUUID))
            throw new GameException("Board doesn't exists!");

        return getGames().get(gameUUID);
    }

    /**
     * Create a new game
     *
     * @param gameUUID   uuid of the game
     * @param name       name of the game
     * @param width      board width
     * @param height     board height
     * @param maxPlayers max players
     * @param player     add that player to the board
     */
    public Game createNewGame(UUID gameUUID, String name, int width, int height, int maxPlayers, Player player) {
        Board board = new Board(width, height);
        Game game = new Game(maxPlayers, name, gameUUID, board);
        game.getPlayerSystem().addPlayer(player);
        games.put(gameUUID, game);
        SocketGlobalService.getClients().forEach(client ->
                client.sendEvent("updatesGamesList", JavalinJson.toJson(
                        TemplateUtil.model(
                                "games", getGames()
                        )
                ))
        );
        return game;
    }

    /**
     * Add player to an existing game.
     *
     * @param gameUUID uuid of the game you want to access
     * @param player   player which will be added
     */
    public Game joinExistingGame(UUID gameUUID, Player player) {
        Game game = getGame(gameUUID);
        game.getPlayerSystem().addPlayer(player);
        SocketGlobalService.getClients().forEach(client ->
                client.sendEvent("updatesGamesList", JavalinJson.toJson(
                        TemplateUtil.model(
                                "games", getGames()
                        )
                ))
        );
        SocketGameService.getClients(gameUUID).forEach(client ->
                client.sendEvent("updatePlayersList", JavalinJson.toJson(
                        TemplateUtil.model(
                                "players", game.getPlayerSystem().getPlayers(),
                                "currentPlayer", game.getPlayerSystem().getCurrentPlayer()
                        )
                ))
        );
        return game;
    }

    /**
     * Check if game is ready and waits for player.
     *
     * @param gameUUID uuid of the game you want to access
     * @return game ready status
     */
    public boolean isReady(UUID gameUUID) {
        return getGame(gameUUID).isReady();
    }

    /**
     * removes the game and all clients
     *
     * @param gameUUID uuid of the game you want to access
     */
    public void deleteGame(UUID gameUUID) {
        games.remove(gameUUID);

        SocketGameService.getInstance().removeClient(gameUUID);

        SocketGlobalService.getClients().forEach(client ->
                client.sendEvent("updatesGamesList", JavalinJson.toJson(
                        TemplateUtil.model(
                                "games", GameService.getGames()
                        )
                ))
        );
    }

    /**
     * Checks if the game is finished
     *
     * @param gameUUID uuid of the game you want to access
     * @return if the game is finished
     */
    public boolean isCompleted(UUID gameUUID) {
        return games.get(gameUUID).isCompleted();
    }
}