/*
 * Copyright (C) 2019 Daniél Kerkmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package projects.pis.team42.domain.service;

import io.javalin.json.JavalinJson;
import io.javalin.rendering.template.TemplateUtil;
import projects.pis.team42.domain.Line;
import projects.pis.team42.domain.Player;
import projects.pis.team42.domain.Reward;
import projects.pis.team42.game.Game;

import javax.inject.Singleton;
import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Service class for interaction between
 * MovementSystem and MovementController
 */
@Singleton
public class MovementService {

    @NotNull
    private static MovementService instance;

    private MovementService() {
    }

    public static MovementService getInstance() {
        return instance = Optional.ofNullable(instance).orElse(new MovementService());
    }

    /**
     * Add player move to game.
     *
     * @param gameUUID uuid of the game you want to access
     * @param line     the coordinate where the move is set
     */
    public void addMove(UUID gameUUID, UUID playerUUID, Line line) {
        Game game = GameService.getGame(gameUUID);
        Player player = game.getPlayerSystem().getPlayer(playerUUID);
        game.getMovementSystem().addMove(player, line);

        SocketGameService.getClients(gameUUID).forEach(client ->
                client.sendEvent("addMove", JavalinJson.toJson(
                        TemplateUtil.model(
                                "moves", game.getMovementSystem().getMoves(),
                                "rewards", game.getRewardSystem().getRewards(),
                                "currentPlayer", game.getPlayerSystem().getCurrentPlayer()
                        )
                ))
        );

        if (game.isCompleted()) {
            SocketGameService.getClients(gameUUID).forEach(client ->
                    client.sendEvent("isComplete", JavalinJson.toJson(
                            TemplateUtil.model(
                                    "rewards", game.getRewardSystem().getRewards()
                                            .entrySet()
                                            .stream()
                                            .sorted(Map.Entry.<Player, Set<Reward>>comparingByValue(Comparator.comparingInt(Set::size)).reversed())
                                            .map(playerSetEntry -> TemplateUtil.model(
                                                    "player", playerSetEntry.getKey(),
                                                    "rewards", playerSetEntry.getValue()
                                            ))
                                            .collect(Collectors.toList())
                            )
                    ))
            );
            GameService.getInstance().deleteGame(gameUUID);
        }

    }

    /**
     * Get all moves from all players.
     *
     * @param gameUUID game uuid
     * @return all moves
     */
    public Map<Player, Set<Line>> getMoves(UUID gameUUID) {
        return GameService.getGame(gameUUID).getMovementSystem().getMoves();
    }
}
