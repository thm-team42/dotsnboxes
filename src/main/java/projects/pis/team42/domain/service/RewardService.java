/*
 * Copyright (C) 2019 Daniél Kerkmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package projects.pis.team42.domain.service;

import projects.pis.team42.domain.Player;
import projects.pis.team42.domain.Reward;

import javax.inject.Singleton;
import javax.validation.constraints.NotNull;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * Service class for interaction between
 * the RewardSystem class and the RewardController
 */
@Singleton
public class RewardService {

    @NotNull
    private static RewardService instance;

    private RewardService() {
    }

    public static RewardService getInstance() {
        return instance = Optional.ofNullable(instance).orElse(new RewardService());
    }

    /**
     * Get all rewards from all players
     *
     * @return all rewards settings point
     */
    public Map<Player, Set<Reward>> getRewards(UUID gameUUID) {
        return GameService.getGame(gameUUID).getRewardSystem().getRewards();
    }
}
