/*
 * Copyright (C) 2019 Daniél Kerkmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package projects.pis.team42.domain;

import lombok.Data;
import projects.pis.team42.utils.GameException;

import javax.validation.constraints.Min;

/**
 * Class representing a rectangular playing board
 * through height and width, which must be >=3.
 * the board is NOT used for storing moves or board state
 */
@Data
public class Board {

    @Min(3)
    private final int width;

    @Min(3)
    private final int height;

    public Board(int width, int height) {
        if (width < 3)
            throw new GameException("The Board.ts is too thin (width).");
        if (height < 3)
            throw new GameException("The Board.ts is too thin (height).");
        if (width > 9)
            throw new GameException("The Board.ts is too big (width).");
        if (height > 9)
            throw new GameException("The Board.ts is too big (height).");

        this.width = width;
        this.height = height;
    }
}
