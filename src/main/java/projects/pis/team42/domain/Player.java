/*
 * Copyright (C) 2019 Daniél Kerkmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package projects.pis.team42.domain;

import lombok.Data;
import projects.pis.team42.utils.GameException;

import javax.validation.constraints.NotNull;
import java.util.UUID;

/**
 * Class representing a player,
 * who is identified through a UUID
 * and has a username
 */
@Data
public class Player {

    @NotNull
    private final UUID uuid;

    @NotNull
    private final String username;

    public Player(UUID uuid, String username) {
        if (username.length() < 3)
            throw new GameException("Username too short!");
        if (username.length() > 20)
            throw new GameException("Username too long!");

        this.uuid = uuid;
        this.username = username;
    }

    @Override
    public String toString() {
        return uuid.toString();
    }
}
