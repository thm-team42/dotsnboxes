/*
 * Copyright (C) 2019 Daniél Kerkmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package projects.pis.team42.utils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Simple class with useful stream-utilities
 */

public final class StreamUtil {

    private StreamUtil() {
    }

    public static <K, R, V extends Collection<R>> Stream<R> getStreamFromMap(Map<K, V> map) {
        return map.values().stream().flatMap(Collection::stream);
    }

    public static <K, R, V extends Collection<R>> Set<R> getSetFromMap(Map<K, V> map) {
        return getStreamFromMap(map).collect(Collectors.toSet());
    }

    public static <K, R, V extends Collection<R>> List<R> getListFromMap(Map<K, V> map) {
        return getStreamFromMap(map).collect(Collectors.toList());
    }
}
