/*
 * Copyright (C) 2019 Daniél Kerkmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package projects.pis.team42;

import io.javalin.Javalin;
import io.javalin.core.util.SwaggerRenderer;
import io.javalin.rendering.template.TemplateUtil;
import io.javalin.validation.JavalinValidation;
import org.eclipse.jetty.server.session.DefaultSessionCache;
import org.eclipse.jetty.server.session.FileSessionDataStore;
import org.eclipse.jetty.server.session.SessionCache;
import org.eclipse.jetty.server.session.SessionHandler;
import projects.pis.team42.utils.GameException;
import projects.pis.team42.web.controller.GameController;
import projects.pis.team42.web.controller.MovementController;
import projects.pis.team42.web.controller.PlayerController;
import projects.pis.team42.web.controller.RewardController;
import projects.pis.team42.web.socket.GameSocket;

import java.io.File;
import java.util.UUID;

import static io.javalin.apibuilder.ApiBuilder.*;

public class Team42 {
    public static void main(String[] args) {
        JavalinValidation.register(UUID.class, UUID::fromString);

        Javalin.create()
                .enableWebJars().get("/api", new SwaggerRenderer("api.yaml"))
                .enableStaticFiles("/public")
                .enableCorsForAllOrigins()
                .routes(() -> {
                    path("/games", () -> {
                        get("/", GameController::getGame);
                        post("/", GameController::createGame);
                        put("/", GameController::joinGame);
                        delete("/", GameController::deleteGame);
                        get("/all", GameController::getGames);

                        get("/ready", GameController::isReady);
                        get("/completed", GameController::isCompleted);
                        path("/players", () -> {
                            get("/", PlayerController::getPlayer);
                            get("/all", PlayerController::getPlayers);
                            get("/current", PlayerController::getCurrentPlayer);
                        });

                        path("/moves", () -> {
                            get("/", MovementController::getMoves);
                            post("/", MovementController::addMove);
                        });

                        path("/rewards", () -> {
                            get("/", RewardController::getRewards);
                        });
                    });
                })
                .exception(GameException.class, (exception, ctx) -> {
                    ctx.status(400);
                    ctx.json(TemplateUtil.model("error", exception.getMessage()));
                })
                .sse("/events/games", GameSocket::onConnectGame)
                .sse("/events", GameSocket::onConnect)
                .sessionHandler(Team42::fileSessionHandler)
                .start(8080);
    }

    /**
     * SessionHandler from Javalin Documentation.
     * https://javalin.io/tutorials/jetty-session-handling-java
     *
     * @return session handler
     */
    private static SessionHandler fileSessionHandler() {
        SessionHandler sessionHandler = new SessionHandler();
        SessionCache sessionCache = new DefaultSessionCache(sessionHandler);
        sessionCache.setSessionDataStore(fileSessionDataStore());
        sessionHandler.setSessionCache(sessionCache);
        sessionHandler.setHttpOnly(true);
        // make additional changes to your SessionHandler here
        return sessionHandler;
    }

    /**
     * SessionHandler from Javalin Documentation.
     * https://javalin.io/tutorials/jetty-session-handling-java
     *
     * @return file session data store
     */
    private static FileSessionDataStore fileSessionDataStore() {
        FileSessionDataStore fileSessionDataStore = new FileSessionDataStore();
        File baseDir = new File(System.getProperty("java.io.tmpdir"));
        File storeDir = new File(baseDir, "javalin-session-store");
        storeDir.mkdir();
        fileSessionDataStore.setStoreDir(storeDir);
        return fileSessionDataStore;
    }
}
