/*
 * Copyright (C) 2019 Daniél Kerkmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package projects.pis.team42.game;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.javalin.json.JavalinJson;
import projects.pis.team42.domain.Line;
import projects.pis.team42.domain.Player;
import projects.pis.team42.domain.service.GameService;
import projects.pis.team42.utils.GameException;

import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * Class belonging to a game
 * and providing functionality for adding
 * and storing moves for players.
 * The moves of a player are stored in a map
 */
public class MovementSystem {

    @NotNull
    @JsonIgnore
    private final Game game;

    @NotNull
    private Map<Player, Set<Line>> moves;

    public MovementSystem(Game game) {
        this.game = Objects.requireNonNull(game);

        moves = new HashMap<>();
    }

    /**
     * method checking if a players move is valid
     * in the current game state and adding it to
     * his moves
     *
     * @param player
     * @param line
     */
    public void addMove(Player player, Line line) {
        if (!game.isReady())
            throw new GameException("Game is not ready!");

        if (!player.equals(game.getPlayerSystem().getCurrentPlayer()))
            throw new GameException("It's not your turn!");

        validatePoint(line);

        if (moves.values().stream().flatMap(Collection::stream).anyMatch(line::equals))
            throw new GameException("There is an existing move!");

        moves.putIfAbsent(game.getPlayerSystem().getCurrentPlayer(), new HashSet<>());
        moves.get(game.getPlayerSystem().getCurrentPlayer()).add(line);

        if (!game.getRewardSystem().addReward(game.getPlayerSystem().getCurrentPlayer(), line))
            game.getPlayerSystem().nextPlayer();
    }

    public Map<Player, Set<Line>> getMoves() {
        return moves;
    }

    /**
     * method that checks if a line is valid on the current board
     *
     * @param line line that should be checked
     * @return original line, if all tests are passed
     */
    private Line validatePoint(Line line) {
        int maxWidth = game.getBoard().getWidth() * 2 - 1;
        int maxHeight = game.getBoard().getHeight() * 2 - 1;

        if (((line.getX() < 0) || (line.getX() > maxWidth)) ||
                ((line.getY() < 0) || (line.getY() > maxHeight)))
            throw new GameException("Point is outside of the board.");

        if (
                ((line.getX() == 0) && (line.getY() == 0)) ||
                        ((line.getX() == 0) && (line.getY() == maxHeight)) ||
                        ((line.getX() == maxWidth) && (line.getY() == 0)) ||
                        ((line.getX() == maxWidth) && (line.getY() == maxHeight))
        )
            throw new GameException("Point is on corner.");

        if ((line.getX() % 2) == (line.getY() % 2))
            throw new GameException("Point is not a move");

        return line;
    }
}
