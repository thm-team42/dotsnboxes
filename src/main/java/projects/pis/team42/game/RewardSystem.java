/*
 * Copyright (C) 2019 Daniél Kerkmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package projects.pis.team42.game;

import com.fasterxml.jackson.annotation.JsonIgnore;
import projects.pis.team42.domain.Line;
import projects.pis.team42.domain.Player;
import projects.pis.team42.domain.Reward;
import projects.pis.team42.utils.StreamUtil;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Class belonging to a game
 * and providing functionality for adding
 * and storing rewards for players.
 * The rewards of a player are stored in a map
 */
public class RewardSystem {

    @NotNull
    @JsonIgnore
    private final Game game;

    @NotNull
    private Map<Player, Set<Reward>> rewards;

    public RewardSystem(Game game) {
        this.game = Objects.requireNonNull(game);

        rewards = new HashMap<>();
    }

    /**
     * Method for checking,
     * if a player-move caused a
     * reward and adding it to the player
     *
     * @param player player that gets the reward
     * @param line move the player did
     */
    public boolean addReward(Player player, Line line) {
        rewards.putIfAbsent(player, new HashSet<>());
        return (checkRewards(line).peek(reward -> rewards.get(player).add(reward)).count() > 0);
    }

    /**
     * Method for getting all rewards
     *
     * @return map of all rewards
     */
    public Map<Player, Set<Reward>> getRewards() {
        return rewards;
    }

    /**
     * Method checking for valid rewards
     * after a player move
     *
     * @param line move that was made
     * @return stream of the new rewards
     */
    private Stream<Reward> checkRewards(Line line) {
        if (line.x % 2 == 1 && line.y % 2 == 0)
            return Stream.of(
                    checkRewardUp(line),
                    checkRewardDown(line)
            ).filter(Optional::isPresent).map(Optional::get);
        if (line.x % 2 == 0 && line.y % 2 == 1)
            return Stream.of(
                    checkRewardRight(line),
                    checkRewardLeft(line)
            ).filter(Optional::isPresent).map(Optional::get);
        return Stream.empty();
    }

    private Optional<Reward> checkRewardUp(Line start) {
        return checkReward(start, new Line(+1, -1), new Line(-1, -1), new Line(+0, -2), new Reward(+0, -1));
    }

    private Optional<Reward> checkRewardRight(Line start) {
        return checkReward(start, new Line(+1, +1), new Line(+1, -1), new Line(+2, +0), new Reward(+1, +0));
    }

    private Optional<Reward> checkRewardDown(Line start) {
        return checkReward(start, new Line(+1, +1), new Line(-1, +1), new Line(+0, +2), new Reward(+0, +1));
    }

    private Optional<Reward> checkRewardLeft(Line start) {
        return checkReward(start, new Line(-1, +1), new Line(-1, -1), new Line(-2, +0), new Reward(-1, +0));
    }

    private Optional<Reward> checkReward(Line start, Line match1, Line match2, Line match3, Reward reward) {
        return checkReward(
                Stream.of(match1, match2, match3)
                        .map(matchLine -> new Line(start.x + matchLine.x, start.y + matchLine.y))
                        .collect(Collectors.toList()),
                Optional.of(start).map(line -> new Reward(line.x + reward.x, line.y + reward.y)).get()
        );
    }

    private Optional<Reward> checkReward(List<Line> match, Reward reward) {
        if (StreamUtil.getListFromMap(game.getMovementSystem().getMoves()).containsAll(match))
            return Optional.of(reward);
        return Optional.empty();
    }
}
