/*
 * Copyright (C) 2019 Daniél Kerkmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package projects.pis.team42.game;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import projects.pis.team42.domain.Player;
import projects.pis.team42.utils.GameException;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Class belonging to a game
 * and providing functionality for adding
 * removing and storing players, as well as
 * other player-related functions for the game
 * The Players a stored in a List
 */
@Data
public class PlayerSystem {

    @NotNull
    @JsonIgnore
    private final Game game;

    @NotNull
    private final List<Player> players;

    private Player currentPlayer;

    @Min(2)
    private final int maxPlayers;

    public PlayerSystem(Game game, int maxPlayers) {
        this.game = Objects.requireNonNull(game);

        this.players = new ArrayList<>();

        if (maxPlayers < 2)
            throw new GameException("There are not enough players!");

        if (maxPlayers > 9)
            throw new GameException("There are too many players!");

        this.maxPlayers = maxPlayers;
    }

    /**
     * Method for adding players to a game (if possible)
     *
     * @param player player to add to the game
     */
    public void addPlayer(Player player) {
        if (isEnoughPlayers())
            throw new GameException("Max player amount exceeded!");

        if (players.contains(player))
            throw new GameException("The player already exists!");

        players.add(player);

        if (currentPlayer == null)
            currentPlayer = player;
    }

    /**
     * Simple method for getting
     * a player through his UUID
     *
     * @param playerUUID uuid of player to get
     * @return the wanted player
     */
    public Player getPlayer(UUID playerUUID) {
        return players.stream()
                .filter(player -> player.getUuid().equals(playerUUID))
                .findFirst().orElseThrow(() -> new GameException("Player not found!"));
    }

    public void removePlayer(UUID playerUUID) {
        removePlayer(getPlayer(playerUUID));
    }

    /**
     * Method for removing a player
     * from a game (if possible)
     *
     * @param player player who should be removed
     */
    public void removePlayer(Player player) {
        if (players.size() <= 0)
            return;

        if (currentPlayer.equals(player) && players.size() > 1)
            nextPlayer();

        players.remove(player);
    }

    /**
     * Checks if the maximum amount
     * of players is reached
     *
     * @return are there enough players
     */
    public boolean isEnoughPlayers() {
        return maxPlayers <= players.size();
    }

    /**
     * Switches the players turn
     *
     * @return the player who's turn it is
     */
    public Player nextPlayer() {
        return currentPlayer = players.get(((players.indexOf(currentPlayer) + 1) % maxPlayers));
    }
}
