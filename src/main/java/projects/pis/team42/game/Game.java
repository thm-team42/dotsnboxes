/*
 * Copyright (C) 2019 Daniél Kerkmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package projects.pis.team42.game;

import lombok.Data;
import projects.pis.team42.domain.Board;
import projects.pis.team42.utils.GameException;
import projects.pis.team42.utils.StreamUtil;

import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.UUID;

/**
 * Class representing a game of Dots 'n Boxes
 */
@Data
public class Game {

    @NotNull
    private final String name;

    @NotNull
    private final UUID uuid;

    @NotNull
    private final Board board;

    @NotNull
    private final PlayerSystem playerSystem;

    @NotNull
    private final MovementSystem movementSystem;

    @NotNull
    private final RewardSystem rewardSystem;

    public Game(int maxPlayers, String name, UUID uuid, Board board) {
        if (name.length() < 3)
            throw new GameException("Board name too short!");
        if (name.length() > 20)
            throw new GameException("Board name too long!");
        this.uuid = Objects.requireNonNull(uuid);
        this.board = Objects.requireNonNull(board);
        this.playerSystem = new PlayerSystem(this, maxPlayers);
        this.movementSystem = new MovementSystem(this);
        this.rewardSystem = new RewardSystem(this);

        this.name = name;
    }

    /**
     * Check if game is ready to play
     *
     * @return game is ready
     */

    public boolean isReady() {
        return playerSystem.isEnoughPlayers();
    }


    /**
     * Checks if game is completed,
     * by comparing the amount of current rewards to the maximum amount of rewards
     *
     * @return the game is completed
     */
    public boolean isCompleted() {
        return StreamUtil.getListFromMap(rewardSystem.getRewards()).size()
                >= (getBoard().getWidth() - 1) * (getBoard().getHeight() - 1);
    }
}