# Dots 'n Boxes - Team42 (Fr. 5.Block/Krümmel)



### About this Project:
>The purpose of this project is to provide a platform for
multiple players to play a game of [Dots 'n Boxes](https://en.wikipedia.org/wiki/Dots_and_Boxes). 
Players can create a room for a game, choose the board size, their name
and the maximum amount of players. 
After a player creates a room, it becomes visible for other players to join.
When the room is full the game starts. For the rules of the game see below.

>For the communication between the different clients we set up server-sent-events
for all the players, so the server can react to client-actions and 
pass them through to the other players. In the backend we use [Project Lombok](https://projectlombok.org/)
for creating more compact and understandable classes. Project Lombok uses
Java's annotation-feature and processes these to create utility-methods for
classes. Our frontend uses [Bootstrap](https://getbootstrap.com/) and has responsive
Design for compatibility with different viewport sizes.
We also implemented [Swagger](https://swagger.io/) for a clearer structure and documentation.

![](screenshot.png)

### Keywords:

    * Bootstrap
    * responsive Design
    * Server-Sent-Events
    * Singletons
    * Lombok
    * Swagger

### Developers:
    
    * Daniél Kerkmann
    * John Beinecke
    * Sadaqat Ahmed

### To run the server:
    * git clone https://gitlab.com/thm-team42/backend
    * cd backend
    * gradle run
    * visit page: http://localhost:8080/
    * Have some fun :)
    
    * Swagger Documentation: http://localhost:8080/api
    * (if you want to play against yourself, open another tab in incognite mode :))

### Game Rules:
    As the game name already says, there are dots on a board. The players take turns
    connecting these dots with lines. When a player connects two dots and closes
    a square of dots with that move, he is rewarded with that box, earning him a point
    and an additional move. The players keep taking turns until there are no more dots
    to connect left.
    The player with the most boxes wins the game.

---

### Project Structure:
#### General:
Our project uses a clear structure of different types of classes:
* **Entity-Classes:** Raw data classes representing simple game elements
* **Game-Classes:** Game logic classes using entities and services
* **Service-Classes:** Singleton classes providing data-processing for game-classes
* **Controller-Classes:** Classes handling client-requests

#### Packages:
    * domain
      -> Board
      -> Line
      -> Player
      -> Reward
      -> service
        - GameService
        - MovementService
        - PlayerService
        - RewardSystem
    
    * game
      -> Game
      -> MovementSystem
      -> PlayerSystem
      -> RewardSystem
      
    * utils
      -> GameException
      -> SessionUtil
      -> StreamUtil
      
    * web
      -> controller
        - GameController
        - MovementController
        - PlayerController
        - RewardController
        
      -> socket
        - GameSocket

---
### Entities:
* **Board: Represents a game board**
  * **Variables:** int width, int height
  
  
* **Player: Represents a player**
  * **Variables:** String username, UUID uuid
  
  
* **Line: Represents a move**
  * **Variables:** int x, int y
  
  
* **Reward: Represents a reward**
  * **Variables:** int x, int y
  
---
### Services (Singletons):
* **GameServices: Offers game-related data-processing**
  * **Variables:** Map<UUID, Game> games, Map<UUID,Set<SseClient>>
  * **Methods:** addClient, getClients, getGameList, getGames, getGame, createNewGame, joinExistingGame, isReady, deleteGame, isCompleted
  
  
* **MovementService: Offers movement-related data-processing**
  * **Methods:** addMove, getMoves
  
  
* **PlayerService: Offers player-related data-processing**
  * **Methods:** getPlayer, getPlayers, getCurrentPlayer, removePlayer
  
  
* **RewardService: Offers reward-related data-processing**
  * **Methods:** getRewards

---
  
### Game:
* **Game: Represents a game of Dots 'n boxes**
  * **Variables:** String name, UUID uuid, Board board, PlayerSystem playerSystem, MovementSystem movementSystem, RewardSystem rewardSystem
  * **Methods:** isReady, isCompleted
  
  
* **MovementSystem: Controls functionality for moves**
  * **Variables:** Map<Player, Set<Line>> moves
  * **Methods:** addMove, validatePoint
  

* **PlayerSystem: Controls functionality for players**
  * **Variables:** List<Players>
  * **Methods:** addPlayer, getPlayer, removePlayers, isEnoughPlayers, nextPlayer
  
  
* **RewardSystem: Controls functionality for rewards**
  * **Variables:** Map<Players,Set<Rewards>> rewards
  * **Method:** addReward, checkRewards, checkRewardUp, checkRewardDown, checkRewardRight, checkRewardLeft, checkReward

---
### Utils:
* **GameException: Simple Exception**


* **SessionUtil: Offers data-processing for sessions**
  * **Methods:** getUUID
  
  
* **StreamUtil: Generic stream-util class**
  * **Methods:** getStreamFromMap, getSetFromMap, getListFromMap
  
---
### Controller:
* **GameController: Handles game-related requests**
  * **Methods:** createGame, joinGame, isReady, isCompleted, deleteGame, getGames, getGame
  
  
* **MovementController: Handles movement-related requests**
  * **Methods:** addMove, getMoves
  
  
* **PlayerController: Handles player-related requests**
  * **Methods:** getPlayer, getPlayers, getCurrentPlayer
  
  
* **RewardController: Handles reward-related requests**
  * **Methods:** getRewards
  
---  
### Socket:
* **GameSocket: WebSocket for reacting to client-actions**

---


Copyright © 2019 Daniél Kerkmann, John Beinecke, Sadaqat Ahmed, code licensed under AGPLv3, source code https://gitlab.com/thm-team42/backend